package com.example.ans.desu.Instruments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

public class UnsharpTask extends AsyncTask<Bundle, Void, Bitmap> {

    OnUnsharpCompleteListener onUnsharpCompleteListener;
    public ProgressDialog dialog;
    Context ctx;

    interface OnUnsharpCompleteListener {
        void unsharpComplete(Bitmap bitmap);
    }

    static private Bitmap getUnsharpedImage(Bitmap bitmap, int radius, int amount, int threshold) {
        Bitmap blurredBitmap = fastblur(bitmap, radius);
        return unsharpMasking(bitmap, blurredBitmap, amount, threshold);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(ctx);
        dialog.setMessage("Загрузка...");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        dialog.show();
    }

    @Override
    protected Bitmap doInBackground(Bundle... bundles) {
        int radius = bundles[0].getInt("radius");
        int amount = bundles[0].getInt("amount");
        int threshold = bundles[0].getInt("threshold");
        Bitmap bitmap = bundles[0].getParcelable("bitmap");
        Log.d("nya", "1");
        return getUnsharpedImage(bitmap, radius, amount, threshold);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        dialog.dismiss();
        Unsharp unsharp = new Unsharp();
        try {
            onUnsharpCompleteListener = unsharp;
        } catch (ClassCastException e) {
            throw new ClassCastException(unsharp.toString() + "must implement OnUnsharpListener");
        }
        Log.d("nya", "2");
        onUnsharpCompleteListener.unsharpComplete(bitmap);
    }

    static private Bitmap unsharpMasking(Bitmap orig, Bitmap blur, int amount, int threshold) {
        int w = orig.getWidth(), h = orig.getHeight();
        int wh = w * h;
        int origPixels[] = new int[wh];
        int blurredPixels[] = new int[wh];
        int returnPixels[] = new int[wh];
        orig.getPixels(origPixels, 0, w, 0, 0, w, h);
        blur.getPixels(blurredPixels, 0, w, 0, 0, w, h);
        int orgRed = 0, orgGreen = 0, orgBlue = 0;
        int blurredRed = 0, blurredGreen = 0, blurredBlue = 0;
        int usmPixel = 0;
        int alpha = 0xFF000000; //transperency is not considered and always zero

        for (int i = 0; i < wh; i++) {
            int origPixel = origPixels[i], blurredPixel = blurredPixels[i];

            //seperate RGB values of original and blurred pixels into seperate R,G and B values
            orgRed = (origPixel & 0xff0000) >> 16;
            orgGreen = (origPixel & 0x00ff00) >> 8;
            orgBlue = (origPixel & 0x0000ff);
            blurredRed = (blurredPixel & 0xff0000) >> 16;
            blurredGreen = (blurredPixel & 0x00ff00) >> 8;
            blurredBlue = (blurredPixel & 0x0000ff);

            //If the absolute val. of difference between original and blurred
            //values are greater than the given threshold add weighed difference
            //back to the original pixel. If the result is outside (0-255),
            //change it back to the corresponding margin 0 or 255
            if (Math.abs(orgRed - blurredRed) >= threshold) {
                orgRed = (int) (amount * (orgRed - blurredRed) + orgRed);
                orgRed = orgRed > 255 ? 255 : orgRed < 0 ? 0 : orgRed;
            }

            if (Math.abs(orgGreen - blurredGreen) >= threshold) {
                orgGreen = (int) (amount * (orgGreen - blurredGreen) + orgGreen);
                orgGreen = orgGreen > 255 ? 255 : orgGreen < 0 ? 0 : orgGreen;
            }

            if (Math.abs(orgBlue - blurredBlue) >= threshold) {
                orgBlue = (int) (amount * (orgBlue - blurredBlue) + orgBlue);
                orgBlue = orgBlue > 255 ? 255 : orgBlue < 0 ? 0 : orgBlue;
            }

            usmPixel = (alpha & origPixel | (orgRed << 16) | (orgGreen << 8) | orgBlue);
            returnPixels[i] = usmPixel;
        }
        Bitmap ret = orig.copy(orig.getConfig(), true);
        ret.setPixels(returnPixels, 0, w, 0, 0, w, h);
        return ret;
    }

    static private Bitmap fastblur(Bitmap sentBitmap, int radius) {


        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) {
            return (sentBitmap);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }
}

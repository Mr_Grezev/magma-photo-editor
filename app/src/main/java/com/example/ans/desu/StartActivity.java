package com.example.ans.desu;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.provider.MediaStore;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StartActivity extends AppCompatActivity {

    Uri photoURI = null;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    Intent intent = new Intent(this, EditorActivity.class);
                    if (photoURI == null) {
                        Uri imageURI = data.getData();
                        Log.d("Gallery", getRealPathFromURI(imageURI));
                        File orig = new File(getRealPathFromURI(imageURI));
                        File newImage = null;
                        try {
                            newImage = createImageFile();
                            copyFile(orig, newImage);

                            Log.d("Gallery", newImage.getPath());
                        } catch (IOException ioe) {
                            Log.d("Gallery", ioe.getLocalizedMessage());
                        }
                        photoURI = FileProvider.getUriForFile(StartActivity.this,
                                "com.example.ans.fileprovider",
                                newImage);
                        intent.putExtra("from", 0);
                    } else {
                        intent.putExtra("from", 1);
                    }

                    intent.putExtra("uri", photoURI);

                    startActivityForResult(intent, 2);
                }
                break;
            case 2:
                photoURI = null;
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        final ImageButton setImage = findViewById(R.id.Button);
        final ImageButton setImage2 = findViewById(R.id.Button2);

        setImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 1);
            }
        });

        setImage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ioe) {
                    Log.d("Camera", ioe.getLocalizedMessage());
                }
                if (photoFile != null) {
                    photoURI = FileProvider.getUriForFile(StartActivity.this,
                            "com.example.ans.fileprovider",
                            photoFile);
                    photoPickerIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(photoPickerIntent, 1);
                }
            }
        });

    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
    }

    private void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }
        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        Cursor cursor = null;
        try {

            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = this.getContentResolver().query(contentUri, proj, null, null, null);
            cursor.moveToFirst();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
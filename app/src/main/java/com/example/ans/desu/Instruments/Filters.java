package com.example.ans.desu.Instruments;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.ans.desu.EditorActivity;
import com.example.ans.desu.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import static com.example.ans.desu.EditorActivity.dipToPixels;
import static com.example.ans.desu.Instruments.ScalAndRota.context;

public class Filters {

    static Map<Integer, MColor> map = new HashMap<Integer, MColor>();
    static int counter = 0;
    static ImageView image;
    static Bitmap original;
    static Context context;


    static private class MColor {
        public int a, r, g, b;

        MColor(int r, int g, int b, int a) {
            this.a = a;
            this.r = r;
            this.g = g;
            this.b = b;
        }
    }


    static public Bitmap colorFilterRBG(Bitmap src, int r, int g, int b) {
        int w = src.getWidth(), h = src.getHeight();
        int wh = w * h;
        int origPixels[] = new int[wh];
        int returnPixels[] = new int[wh];
        src.getPixels(origPixels, 0, w, 0, 0, w, h);
        int orgRed = 0, orgGreen = 0, orgBlue = 0;
        int usmPixel = 0;
        int alpha = 0xFF000000;

        for (int i = 0; i < wh; i++) {
            int origPixel = origPixels[i];

            orgRed = (origPixel & 0xff0000) >> 16;
            orgGreen = (origPixel & 0x00ff00) >> 8;
            orgBlue = (origPixel & 0x0000ff);

            orgRed = (r != 0) ? r : orgRed;
            orgGreen = (g != 0) ? g : orgGreen;
            orgBlue = (b != 0) ? b : orgBlue;

            usmPixel = (alpha & origPixel | (orgRed << 16) | (orgGreen << 8) | orgBlue);
            returnPixels[i] = usmPixel;
        }
        Bitmap ret = src.copy(src.getConfig(), true);
        ret.setPixels(returnPixels, 0, w, 0, 0, w, h);
        return ret;
    }

    static public Bitmap colorFilterBAW(Bitmap src) {
        int w = src.getWidth(), h = src.getHeight();
        int wh = w * h;
        int origPixels[] = new int[wh];
        int returnPixels[] = new int[wh];
        src.getPixels(origPixels, 0, w, 0, 0, w, h);
        int orgRed = 0, orgGreen = 0, orgBlue = 0;
        int usmPixel = 0;
        int alpha = 0xFF000000; //transperency is not considered and always zero

        for (int i = 0; i < wh; i++) {
            int origPixel = origPixels[i];

            orgRed = (origPixel & 0xff0000) >> 16;
            orgGreen = (origPixel & 0x00ff00) >> 8;
            orgBlue = (origPixel & 0x0000ff);

            int rainbow = ((orgRed + orgGreen + orgBlue) / 3);

            usmPixel = (alpha & origPixel | (rainbow << 16) | (rainbow << 8) | rainbow);
            returnPixels[i] = usmPixel;
        }
        Bitmap ret = src.copy(src.getConfig(), true);
        ret.setPixels(returnPixels, 0, w, 0, 0, w, h);
        return ret;
    }

    static public Bitmap colorFilterNEG(Bitmap src) {
        int w = src.getWidth(), h = src.getHeight();
        int wh = w * h;
        int origPixels[] = new int[wh];
        int returnPixels[] = new int[wh];
        src.getPixels(origPixels, 0, w, 0, 0, w, h);
        int orgRed = 0, orgGreen = 0, orgBlue = 0;
        int usmPixel = 0;
        int alpha = 0xFF000000; //transperency is not considered and always zero

        for (int i = 0; i < wh; i++) {
            int origPixel = origPixels[i];

            orgRed = (origPixel & 0xff0000) >> 16;
            orgGreen = (origPixel & 0x00ff00) >> 8;
            orgBlue = (origPixel & 0x0000ff);


            usmPixel = (alpha & origPixel | ((255 - orgRed) << 16) | ((255 - orgGreen) << 8) | (255 - orgBlue));
            returnPixels[i] = usmPixel;
        }
        Bitmap ret = src.copy(src.getConfig(), true);
        ret.setPixels(returnPixels, 0, w, 0, 0, w, h);
        return ret;
    }

    static public Bitmap colorFilterGR(Bitmap src) {
        int w = src.getWidth(), h = src.getHeight();
        int wh = w * h;
        int origPixels[] = new int[wh];
        int returnPixels[] = new int[wh];
        src.getPixels(origPixels, 0, w, 0, 0, w, h);
        int orgRed = 0, orgGreen = 0, orgBlue = 0;
        int usmPixel = 0;
        int alpha = 0xFF000000; //transperency is not considered and always zero

        for (int i = 0; i < wh; i++) {
            int origPixel = origPixels[i];

            orgRed = (origPixel & 0xff0000) >> 16;
            orgGreen = (origPixel & 0x00ff00) >> 8;
            orgBlue = (origPixel & 0x0000ff);

            int rainbow = ((orgRed + orgGreen + orgBlue) / 3);

            orgRed = rainbow + 40;
            orgGreen = rainbow + 20;
            orgBlue = rainbow - 10;

            if (orgRed > 255) {
                orgRed = 255;
            }
            if (orgGreen > 255) {
                orgGreen = 255;
            }
            if (orgBlue < 0) {
                orgBlue = 0;
            }

            usmPixel = (alpha & origPixel | (orgRed << 16) | (orgGreen << 8) | orgBlue);
            returnPixels[i] = usmPixel;
        }
        Bitmap ret = src.copy(src.getConfig(), true);
        ret.setPixels(returnPixels, 0, w, 0, 0, w, h);
        return ret;
    }

    static public void showFilters(Context context, ImageView image, LinearLayout parent) {
        Filters.image = image;
        Filters.context = context;
        original = ((BitmapDrawable) image.getDrawable()).getBitmap();
        original = original.copy(original.getConfig(), true);
        int size = (int) dipToPixels(context, 70);
        Bitmap orig = original.copy(original.getConfig(),true);
        Bitmap thumb = Bitmap.createScaledBitmap(orig, size, size, false);
        LinearLayout layout = parent;

        map.put(0, new MColor(255, 0, 0, 255));
        map.put(1, new MColor(0, 255, 0, 255));
        map.put(2, new MColor(0, 0, 255, 255));

        int sizeright = (int) dipToPixels(context, 5);
        int sizeleft = (int) dipToPixels(context, 5);

        for (int i = 0; i < map.size(); i++) {
            Bitmap tempRBG = thumb.copy(thumb.getConfig(), true);
            MColor mColor = map.get(counter++);
            int r = mColor.r;
            int g = mColor.g;
            int b = mColor.b;

            tempRBG = colorFilterRBG(tempRBG, r, g, b);
            ImageButton button = new ImageButton(context);
            button.setId(i + 100);
            button.setImageBitmap(tempRBG);
            button.setOnClickListener(onClickRGB);
            button.setBackground(null);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(sizeleft, 0, sizeright, 0);
            button.setLayoutParams(lp);
            layout.addView(button);


        }


        final ImageButton buttonBAW = new ImageButton(context);
        Bitmap tempBAW = thumb.copy(thumb.getConfig(), true);
        tempBAW = colorFilterBAW(tempBAW);
        buttonBAW.setImageBitmap(tempBAW);
        buttonBAW.setOnClickListener(onClickBAW);
        buttonBAW.setBackground(null);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(sizeleft, 0, sizeright, 0);
        buttonBAW.setLayoutParams(lp);
        layout.addView(buttonBAW);

        final ImageButton buttonNEG = new ImageButton(context);
        Bitmap tempNEG = thumb.copy(thumb.getConfig(), true);
        tempNEG = colorFilterNEG(tempNEG);
        buttonNEG.setImageBitmap(tempNEG);
        buttonNEG.setOnClickListener(onClickNEG);
        buttonNEG.setBackground(null);

        lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(sizeleft, 0, sizeright, 0);
        buttonNEG.setLayoutParams(lp);
        layout.addView(buttonNEG);

        final ImageButton buttonGR = new ImageButton(context);
        Bitmap tempGR = thumb.copy(thumb.getConfig(), true);
        tempGR = colorFilterGR(tempGR);
        buttonGR.setImageBitmap(tempGR);
        buttonGR.setOnClickListener(onClickGR);
        buttonGR.setBackground(null);

        lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(sizeleft, 0, sizeright, 0);
        buttonGR.setLayoutParams(lp);
        layout.addView(buttonGR);

        final ImageButton buttonLazy = new ImageButton(context);
        Bitmap tempLazy = thumb.copy(thumb.getConfig(), true);
        tempLazy = colorFilterGR(tempLazy);
        buttonLazy.setImageBitmap(tempLazy);
        buttonLazy.setOnClickListener(onClickLazy);
        buttonLazy.setBackground(null);

        lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(sizeleft, 0, sizeright, 0);
        buttonLazy.setLayoutParams(lp);
        layout.addView(buttonLazy);
        counter = 0;
    }

    static private View.OnClickListener onClickRGB = new View.OnClickListener() {
        @Override
        public void onClick(View view) {


            Bitmap orig = original.copy(original.getConfig(),true);
            Bitmap bitmapRBG = orig.copy(orig.getConfig(), true);
            int id = (view.getId()) - 100;
            MColor mColor = map.get(id);
            Log.d("bm", String.valueOf(id));
            int r = mColor.r;
            int g = mColor.g;
            int b = mColor.b;
            bitmapRBG = colorFilterRBG(bitmapRBG, r, g, b);
            image.setImageBitmap(bitmapRBG);


        }
    };

    static private View.OnClickListener onClickBAW = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Bitmap orig = original.copy(original.getConfig(),true);
            Bitmap bitmapBAW;
            bitmapBAW = colorFilterBAW(orig);
            image.setImageBitmap(bitmapBAW);

        }
    };

    static private View.OnClickListener onClickNEG = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Bitmap orig = original.copy(original.getConfig(),true);
            Bitmap bitmapNEG;
            bitmapNEG = colorFilterNEG(orig);
            image.setImageBitmap(bitmapNEG);

        }
    };
    static private View.OnClickListener onClickGR = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Bitmap orig = original.copy(original.getConfig(),true);
            Bitmap bitmapGR;
            bitmapGR = colorFilterGR(orig);
            image.setImageBitmap(bitmapGR);

        }
    };

    static private View.OnClickListener onClickLazy = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Bitmap orig = original.copy(original.getConfig(),true);
            Bitmap bitmapGR;
            bitmapGR = colorFilterGR(orig);
            image.setImageBitmap(bitmapGR);
            Bundle bundle = new Bundle();
            bundle.putInt("radius", 23);
            bundle.putInt("amount", 9);
            bundle.putInt("threshold", 0);
            bundle.putParcelable("bitmap", bitmapGR);
            UnsharpTask unsharpTask = new UnsharpTask();
            unsharpTask.ctx = Filters.context;
            unsharpTask.execute(bundle);
        }
    };


    static public void clearAll() {
        image = null;
        counter = 0;
        map.clear();
    }

    static  public Bitmap getBitmap(){
        return original.copy(original.getConfig(),false);
    }
}
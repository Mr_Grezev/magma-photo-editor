package com.example.ans.desu.Instruments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.ans.desu.R;

public class ScalingDialogFragment extends DialogFragment {

    public interface OnMassListener{
        void massEvent(double exp);
    }

    OnMassListener onMassListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View viewM = inflater.inflate(R.layout.fragment_dialog_scaling,container);

        onMassListener = new ScalAndRota();

        int h = getArguments().getInt("height");
        int w = getArguments().getInt("width");

        TextView size = viewM.findViewById(R.id.size);
        String sizeText = "Текущий размер: " + String.valueOf(w) + "x" + String.valueOf(h);
        size.setText(sizeText);

        Button ok = viewM.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText text = viewM.findViewById(R.id.coef);
                double coef = 0d;
                if (!text.getText().toString().equals(""))
                    coef = Double.parseDouble(text.getText().toString());
                onMassListener.massEvent(coef);
                dismiss();
            }
        });

        Button cancel = viewM.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return viewM;
    }
}

package com.example.ans.desu;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.ans.desu.Instruments.Filters;
import com.example.ans.desu.Instruments.ScalAndRota;
import com.example.ans.desu.Instruments.ScalingDialogFragment;
import com.example.ans.desu.Instruments.Retouch;
import com.example.ans.desu.Instruments.Unsharp;
import com.example.ans.desu.Instruments.UnsharpMaskingDialogFragment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


/**
 * Created by ans on 28.04.2018.
 */

public class EditorActivity extends AppCompatActivity implements UnsharpMaskingDialogFragment.OnUnsharpListener {

    ImageView image;
    MenuItem save;
    MenuItem previous;
    MenuItem next;
    Uri imageUri;
    int qure_file = 0;
    int max_file = 0;
    int sig = 0;
    static boolean isColorPressed = false;
    static boolean isScalAndRota = false;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        image = findViewById(R.id.image);
        final LinearLayout layout = findViewById(R.id.upstring);


        File dir = new File(Environment.getExternalStorageDirectory() + "/Pictures/MagmaPhoto");
        if (!dir.exists()) {
            dir.mkdir();
        }
        File dis = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.example.ans.desu/files/Pictures/temp");
        if (!dis.exists()) {
            dis.mkdir();
        }
        File del = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.example.ans.desu/files/Pictures/temp", "temp" + 0 + ".jpg");
        for (int i = 1; del.exists(); i++) {
            del.delete();
            del = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.example.ans.desu/files/Pictures/temp", "temp" + i + ".jpg");

        }

        try {
            imageUri = getIntent().getParcelableExtra("uri");
            InputStream imageStream = getContentResolver().openInputStream(imageUri);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(imageStream, null, options);
            try {
                imageStream.close();
            } catch (NullPointerException e) {
                Log.d("stream", e.getLocalizedMessage());
            }
            Log.d("check", "ok");
            int outWidth = options.outWidth;
            int outHeight = options.outHeight;
            if (outWidth <= 50 && outHeight <= 50) {
                Toast.makeText(this, "Картинка не может быть загружена, т.к. она меньше разрешенного размера.", Toast.LENGTH_SHORT).show();
                finish();
            }
            int desiredWidth = 1280;
            int desiredHeight = 720;
            int scale;
            Log.d("check", String.valueOf(outWidth));
            Log.d("check", String.valueOf(outHeight));
            if (outWidth > outHeight) {
                scale = Math.round((float) outHeight / (float) desiredHeight);
            } else {
                scale = Math.round((float) outWidth / (float) desiredWidth);
            }
            options = new BitmapFactory.Options();
            options.inSampleSize = scale;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            imageStream = getContentResolver().openInputStream(imageUri);
            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream, null, options);
            image.setImageBitmap(selectedImage);
            try {
                imageStream.close();
            } catch (NullPointerException e) {
                Log.d("stream", e.getLocalizedMessage());
            }
            if (getIntent().getIntExtra("from", -1) == 0) {
                File file = new File(imageUri.getPath());
                file.delete();
            }

            ImageButton unsharp = findViewById(R.id.sharpnessButton);
            unsharp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogFragment fragment = new UnsharpMaskingDialogFragment();
                    fragment.show(getFragmentManager(), "missiles");
                    save.setEnabled(true);
                    preSave();
                }
            });

            final ImageButton filters = findViewById(R.id.filtersButton);

            filters.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!isColorPressed) {
                        removeViews(image,layout);
                        Filters.showFilters(EditorActivity.this, image, layout);
                        save.setEnabled(true);
                        preSave();
                        isColorPressed = true;
                    } else {
                        layout.removeAllViews();
                        isColorPressed = false;
                        Filters.clearAll();
                    }


                }
            });

            Unsharp.image = image;

            final ImageButton scalandrota = findViewById(R.id.sarButton);
            scalandrota.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isScalAndRota) {
                        removeViews(image,layout);
                        ScalAndRota.showSAR(EditorActivity.this, layout, image, getFragmentManager());
                        save.setEnabled(true);
                        preSave();
                        isScalAndRota = true;

                    } else {
                        layout.removeAllViews();
                        isScalAndRota = false;

                    }
                }
            });

            final ImageButton retouch = findViewById(R.id.retouchButton);
            retouch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Retouch.showRetouch(EditorActivity.this, image, layout);
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

        image.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                return true;
            }
        });


    }


    public static float dipToPixels(Context context, float dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (image.getDrawable().getIntrinsicWidth() <= 50) {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        save = menu.findItem(R.id.save);
        previous = menu.findItem(R.id.previous);
        next = menu.findItem(R.id.next);
        return true;
    }

    static void removeViews(ImageView image, LinearLayout layout) {
        if (isColorPressed) {
            layout.removeAllViews();
            isColorPressed = false;
            Filters.clearAll();
        }

        if (isScalAndRota) {
            layout.removeAllViews();
            isScalAndRota = false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.save:

                File dir = new File(Environment.getExternalStorageDirectory() + "/Pictures/MagmaPhoto");
                String gov = SavePicture(image, dir.getAbsolutePath());
                if (gov.isEmpty())
                    Toast.makeText(this, gov, Toast.LENGTH_LONG).show();
                else Toast.makeText(this, "Saved", Toast.LENGTH_LONG).show();
                save.setEnabled(false);
                break;
            case R.id.previous:
                if (isColorPressed) {
                    LinearLayout layout = findViewById(R.id.upstring);
                    layout.removeAllViews();
                    Filters.clearAll();
                }
                if ((next.isEnabled() == false) && (sig == 1)) {
                    SaveRegular(image);
                }
                next.setEnabled(true);
                FileInputStream in = null;
                qure_file = qure_file - 1;
                if (qure_file == 0) {
                    previous.setEnabled(false);
                }
                try {

                    File download = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.example.ans.desu/files/Pictures/temp/temp" + (qure_file) + ".jpg");
                    in = new FileInputStream(download);
                    image.setImageBitmap(BitmapFactory.decodeStream(in, null, null));
                } catch (IOException ioe) {
                    Log.d("error", ioe.getLocalizedMessage());
                }

                break;
            case R.id.next:
                if (isColorPressed) {
                    LinearLayout layout = findViewById(R.id.upstring);
                    layout.removeAllViews();
                    Filters.clearAll();
                }
                sig = 0;
                qure_file = qure_file + 1;
                if (qure_file == max_file) {
                    next.setEnabled(false);
                }
                previous.setEnabled(true);
                try {
                    File download = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.example.ans.desu/files/Pictures/temp/temp" + (qure_file) + ".jpg");
                    in = new FileInputStream(download);
                    image.setImageBitmap(BitmapFactory.decodeStream(in, null, null));
                } catch (IOException ioe) {
                    Log.d("error", ioe.getLocalizedMessage());
                }
                break;
        }
        return true;
    }

    private String SavePicture(ImageView iv, String folderToSave) {


        OutputStream fOut = null;
        Time time = new Time();
        time.setToNow();
        String path = "";

        try {
            File file = new File(folderToSave, Integer.toString(time.year) + Integer.toString(time.month) + Integer.toString(time.monthDay) + Integer.toString(time.hour) + Integer.toString(time.minute) + Integer.toString(time.second) + ".jpg");
            path = file.getPath();
            fOut = new FileOutputStream(file);
            Bitmap bitmap = ((BitmapDrawable) iv.getDrawable()).getBitmap();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
            fOut.flush();
            fOut.close();
            MediaScannerConnection.scanFile(this, new String[]{file.getAbsolutePath()}, null, null);
        } catch (IOException ioe) {
            Log.d("error", ioe.getLocalizedMessage());
            return "";
        }
        return path;
    }

    private void preSave() {
        SaveRegular(image);
        sig = 1;
        next.setEnabled(false);
        max_file = qure_file = qure_file + 1;
        previous.setEnabled(true);
        if (qure_file != max_file) {
            File del = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.example.ans.desu/files/Pictures/temp", "temp" + String.valueOf(qure_file + 1) + ".jpg");
            for (int i = qure_file + 2; del.exists(); i++) {
                del.delete();
                del = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.example.ans.desu/files/Pictures/temp", "temp" + String.valueOf(i) + ".jpg");
            }
        }
    }

    private void SaveRegular(ImageView im) {
        OutputStream Out = null;
        try {
            File file = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.example.ans.desu/files/Pictures/temp", "temp" + qure_file + ".jpg");
            Out = new FileOutputStream(file);
            Bitmap bitmap = ((BitmapDrawable) im.getDrawable()).getBitmap();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, Out);
            Out.flush();
            Out.close();
        } catch (IOException ioe) {

            Log.d("error", ioe.getLocalizedMessage());
        }
    }

    @Override
    public void unsharpEvent(int radius, int amount, int threshold) {
        Unsharp.unsharp(this, image, radius, amount, threshold);
    }
}

//Большинство фун-ий скопировано(со StackOverflow) и изменено

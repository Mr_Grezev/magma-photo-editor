package com.example.ans.desu.Instruments;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.ans.desu.R;

public class UnsharpMaskingDialogFragment extends DialogFragment {

    int radius, amount, threshold;

    public interface OnUnsharpListener{
        void unsharpEvent(int radius, int amount, int threshold);
    }

    OnUnsharpListener onUnsharpListener;

    public void onAttach(Activity activity){
        super.onAttach(activity);
        try{
            onUnsharpListener = (OnUnsharpListener) activity;
        }
        catch (ClassCastException e){
            throw new ClassCastException(activity.toString() + "must implement OnUnsharpListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dialog_unsharp_masking, container);
        final SeekBar radiusBar = view.findViewById(R.id.radius);
        final SeekBar amountBar = view.findViewById(R.id.amount);
        final SeekBar thresholdBar = view.findViewById(R.id.threshold);
        final Button ok = view.findViewById(R.id.ok);
        final Button cancel = view.findViewById(R.id.cancel);
        radius = radiusBar.getProgress();
        amount = amountBar.getProgress();
        threshold = thresholdBar.getProgress();
        radiusBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                radius = seekBar.getProgress();
                String text = "Радиус: " + String.valueOf(radius);
                ((TextView)view.findViewById(R.id.radius_text)).setText(text);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        amountBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                amount = seekBar.getProgress();
                String text = "Эффект: " + String.valueOf(amount);
                ((TextView)view.findViewById(R.id.amount_text)).setText(text);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        thresholdBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                threshold = seekBar.getProgress();
                String text = "Порог: " + String.valueOf(threshold);
                ((TextView)view.findViewById(R.id.threshold_text)).setText(text);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onUnsharpListener.unsharpEvent(radius,amount,threshold);
                dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return view;
    }


}

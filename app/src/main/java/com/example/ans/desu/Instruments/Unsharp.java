package com.example.ans.desu.Instruments;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.widget.ImageView;

public class Unsharp implements UnsharpTask.OnUnsharpCompleteListener {


    static public Bitmap getUnsharpedImage(Bitmap bitmap, int radius, int amount, int threshold){
        Log.d("nya", String.valueOf(radius));
        return bitmap;
}
    static public ImageView image;

    @Override
    public void unsharpComplete(Bitmap bitmap) {
        image.setImageBitmap(bitmap);
    }

    static public void unsharp(Context context, ImageView image, int radius, int amount, int threshold){
        Unsharp.image = image;
        Bitmap orig = ((BitmapDrawable)image.getDrawable()).getBitmap();
        Bundle bundle = new Bundle();
        bundle.putInt("radius", radius);
        bundle.putInt("amount", amount);
        bundle.putInt("threshold", threshold);
        bundle.putParcelable("bitmap", orig);
        UnsharpTask unsharpTask = new UnsharpTask();
        unsharpTask.ctx = context;
        unsharpTask.execute(bundle);

    }
}

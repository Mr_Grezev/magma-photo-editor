package com.example.ans.desu.Instruments;

import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.ans.desu.EditorActivity;
import com.example.ans.desu.R;

import java.lang.reflect.Array;

import static com.example.ans.desu.EditorActivity.dipToPixels;

public class ScalAndRota implements ScalingDialogFragment.OnMassListener, RotateDialogFragment.OnRotateListener {

    static ImageView image;
    static Context context;
    static FragmentManager fragmentManager;


    public void massEvent(double exp) {
        Bitmap orig = ((BitmapDrawable) image.getDrawable()).getBitmap();
        int h = orig.getHeight();
        int w = orig.getWidth();
        if ((h * exp > 720 && w * exp > 1280) || w * exp < 0 || h * exp < 0 || exp == 0) {
            Toast.makeText(context, "Неверный коэфицент.", Toast.LENGTH_LONG).show();
            return;
        }
        int[] origPixels = new int[w * h];
        int nw = (int) Math.floor(w * exp);
        int nh = (int) Math.floor(h * exp);
        Log.d("x", String.valueOf(nw));
        Log.d("x", String.valueOf(nh));
        Log.d("y", String.valueOf(w));
        Log.d("y", String.valueOf(h));

        int[] newPixels = new int[(int)(Math.floor(w * exp) + (Math.floor(h * exp) * Math.floor(w * exp)))];

        nw--;
        orig.getPixels(origPixels, 0, w, 0, 0, w, h);
        for (int x = 1; x < w; x++) {
            for (int y = 0; y < h; y++) {
                int nx = (int) Math.floor(x * exp);
                int ny = (int) Math.floor(y * exp);
                newPixels[(ny * nw) + nx] = origPixels[(y * w) + x];
                if (exp > 1) {
                    for (int i = 0; i <= (int) exp; i++) {
                        for (int j = 0; j <= (int) exp; j++) {
                            int endl = (ny * nw) + nx + i;
                            int endb = endl + (j * nw);
                            if ((endl < (ny * nw) + nw) && (endb < nw * nh)) {
                                newPixels[endb] = origPixels[(y * w) + x];
                            }
                        }
                    }
                }
            }
        }
        Log.d("kill", String.valueOf(newPixels[0]));
        Bitmap scale = Bitmap.createBitmap(newPixels, nw, nh, Bitmap.Config.ARGB_8888);
        image.setImageBitmap(scale);
    }

    @Override
    public void rotateEvent(int exp) {
        Bitmap orig = ((BitmapDrawable) image.getDrawable()).getBitmap();
        int h = orig.getHeight();
        int w = orig.getWidth();
        int cx = w/2;
        int cy = h/2;
        int minx = Integer.MAX_VALUE, miny = Integer.MAX_VALUE,
                maxx = Integer.MIN_VALUE, maxy = Integer.MIN_VALUE;
        int[] origPixels = new int[w * h];
        orig.getPixels(origPixels, 0, w, 0, 0, w, h);
        int[][] coords = new int[w * h][2];
        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                coords[y * w + x][0] = (int)(cx + (x - cx) * Math.cos(exp) - (y - cy) * Math.sin(exp));
                coords[y * w + x][1] = (int)(cy + (y - cy) * Math.cos(exp) + (x - cx) * Math.sin(exp));
                if (coords[y * w + x][0] > maxx) maxx = coords[y * w + x][0];
                if (coords[y * w + x][1] > maxy) maxy = coords[y * w + x][1];
                if (coords[y * w + x][0] < minx) minx = coords[y * w + x][0];
                if (coords[y * w + x][1] < miny) miny = coords[y * w + x][1];
            }
        }
        int px = (minx < 0) ? Math.abs(minx) : 0;
        int py = (miny < 0) ? Math.abs(miny) : 0;
        int nw = maxx + px;
        int nh = maxy + py;
        if (1600 * 720 < nw * nh) return; //Такое разрешение, чтобы дать больше свободы при повороте
        int[] newPixels = new int[nw * nh];
        nh--; nw--;
        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                int nx = coords[y * w + x][0] + px;
                int ny = coords[y * w + x][1] + py;
                newPixels[(ny * nw) + nx] = origPixels[(y * w) + x];
            }
        }
        Log.d("x", String.valueOf(nw));
        Log.d("y", String.valueOf(nh));
        Bitmap rotate = Bitmap.createBitmap(newPixels, nw, nh, Bitmap.Config.ARGB_8888);
        image.setImageBitmap(rotate);
    }

    static public void showSAR(Context context, LinearLayout parent, ImageView image, FragmentManager fragmentManager) {

        ScalAndRota.image = image;
        ScalAndRota.context = context;
        ScalAndRota.fragmentManager = fragmentManager;

        int sizeleft = (int) dipToPixels(context, 80);

        LinearLayout layout = parent;
        ImageButton buttonRotate = new ImageButton(context);
        buttonRotate.setImageDrawable(context.getDrawable(android.R.drawable.stat_notify_sync_noanim));

        buttonRotate.setOnClickListener(onClickRotate);
        buttonRotate.setBackground(null);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(sizeleft, 0, 0, 0);
        buttonRotate.setLayoutParams(lp);
        layout.addView(buttonRotate);


        ImageButton buttonScaling = new ImageButton(context);
        buttonScaling.setImageDrawable(context.getDrawable(android.R.drawable.ic_menu_crop));

        buttonScaling.setOnClickListener(onClickScaling);
        buttonScaling.setBackground(null);

        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp2.setMargins(sizeleft, 0, 0, 0);
        buttonScaling.setLayoutParams(lp2);
        layout.addView(buttonScaling);


    }


    static private View.OnClickListener onClickRotate = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            RotateDialogFragment fragment=new RotateDialogFragment();
            fragment.show(ScalAndRota.fragmentManager, "rotate");
        }
    };


    static private View.OnClickListener onClickScaling = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ScalingDialogFragment fragment = new ScalingDialogFragment();
            Bitmap bitmap = ((BitmapDrawable) image.getDrawable()).getBitmap();
            Bundle bundle = new Bundle();
            int height = bitmap.getHeight();
            int width = bitmap.getWidth();
            bundle.putInt("height", height);
            bundle.putInt("width", width);
            fragment.setArguments(bundle);
            fragment.show(ScalAndRota.fragmentManager, "scaling");
        }
    };

}
